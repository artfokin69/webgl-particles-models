uniform float time;
uniform float progress;
uniform sampler2D texture1;
uniform sampler2D texture2;
uniform vec2 pixels;
uniform vec2 uvRate1;
uniform vec2 accel;

varying float distance;

void main(){
  vec3 pantone1 = vec3(249., 229., 71.) / 255.;
  vec3 pantone2 = vec3(151., 153., 155.) / 255.;
  vec3 color = mix(pantone1, pantone2, distance);
  gl_FragColor =  vec4(color, 1.0);
}