import { GLTFLoader, GLTF } from 'three/examples/jsm/loaders/GLTFLoader';
import {TextureLoader, Texture, Object3D, Mesh, AudioLoader} from 'three';

const gltfLoader = new GLTFLoader();
const textureLoader = new TextureLoader();
const audioLoader = new AudioLoader();

export class Inertia{
  acc: number;
  friction: number;
  minV: number;
  maxV: number;
  bounce: number;
  #delta: number;
  public value: number;

  constructor(min:number, max:number, acceleration:number, friction:number, bounce:number){
    this.acc = acceleration;  
    this.friction = friction;
    this.minV = min;
    this.maxV = max;
    this.bounce = -Math.abs(bounce);
    this.#delta = 0;
    this.value = min;
  }
  update(input: number){
    this.#delta += (input - this.value) * this.acc;
    this.#delta *= this.friction;
    this.value += this.#delta;
    if (this.value < this.minV) {
       this.value = this.minV;
       if(this.#delta < 0){
           this.#delta *= this.bounce;
       }
    } else
    if (this.value > this.maxV) {
       this.value = this.maxV;
       if(this.#delta > 0){
           this.#delta *= this.bounce;
       }
    }
    return this.value;
  }
  setValue (input:number) {
    this.#delta = 0;
    this.value = Math.min(this.maxV, Math.min(this.minV, input));
    return this.value;
  }
}

export function loadScene(path): Promise<GLTF>{
  return new Promise((resolve,reject)=>{
    gltfLoader.load(path, function ( gltf ) {
      console.log(gltf)
      resolve(gltf);
    }, function ( xhr ) {
      console.log( ( xhr.loaded / xhr.total * 100 ) + '% loaded' );
    }, function ( error ) {
      console.error( error );
      reject();
    } );
  })
}
export function loadTexture(path): Promise<Texture>{
  return new Promise((resolve,reject)=>{
    textureLoader.load(path, function ( texture ) {
      resolve(texture);
    }, function ( xhr ) {
      console.log( ( xhr.loaded / xhr.total * 100 ) + '% loaded' );
    }, function ( error ) {
      console.error( error );
      reject();
    } );
  })
}
export function loadAudio(path): Promise<AudioBuffer>{
  return new Promise((resolve,reject)=>{
    audioLoader.load(path, function ( buffer ) {
      resolve(buffer);
    }, function ( xhr ) {
      console.log( ( xhr.loaded / xhr.total * 100 ) + '% loaded' );
    }, function ( error ) {
      console.error( error );
      reject();
    } );
  })
}

export function addHandlerForEachChildrenMesh(children: (Object3D | Mesh)[], handler: (m: Object3D)=>void){
  children.forEach(child=>{
    if(child.type === 'Object3d'){
      handler(child)
    }
    if(['Mesh','SkinnedMesh'].includes(child.type)){
      handler(child)
    } else{
      addHandlerForEachChildrenMesh(child.children, handler);
    }
  });
}