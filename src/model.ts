import { Group, Scene, Clock, BufferGeometry, BufferAttribute, Mesh, MeshBasicMaterial, MeshPhongMaterial, Raycaster, Object3D, ShaderMaterial, DoubleSide, Vector2, Points, PointsMaterial, Shader, PlaneGeometry, Vector3, Box3, MathUtils, UniformsLib, UniformsUtils } from "three";
import { GLTF } from 'three/examples/jsm/loaders/GLTFLoader';
import autoBind from 'auto-bind';
import {loadScene, addHandlerForEachChildrenMesh} from './helpers';
import ParticlesVertex from '../assets/particles.vert';
import ParticlesFrag from '../assets/particles.frag';
import {GUI} from 'dat.gui';

export default class Model{
  gltf: GLTF;
  figureMesh: Points<BufferGeometry, ShaderMaterial>;
  plane: Mesh<PlaneGeometry, MeshBasicMaterial>;
  loaded = false;
  isInit = false;
  positions: Float32Array;
  mousePos = new Vector3(0,0,0);
  boundingBox = new Box3();
  constructor(){
    autoBind(this);
  }
  async load(){
    this.gltf = await loadScene('./christmas_deer/scene.gltf');
    this.loaded = true;
  }
  init(scene: Scene){
    let firstNode = true;
    this.gltf.scene.traverse(node=>{ 
      if(node['isMesh']){
        if(firstNode){
          node.remove();
        } else{
          // node.material.wireframe = true;
          this.positions = node.geometry.attributes.position.array;
        }
        firstNode = false
      }
    })
    this.figureMesh = this.makeFigure();
    scene.add(this.figureMesh);
    this.setBoundingBox();
    // this.centeredFigure();
    this.plane = this.makePlane();
    scene.add(this.plane);
    this.isInit = true;
  }
  makeFigure(){
    const geometry = new BufferGeometry();
    geometry.setAttribute('position', new BufferAttribute(this.positions, 3));
    const lightsUniforms = UniformsUtils.merge( [
      UniformsLib[ "ambient" ],
      UniformsLib[ "lights" ]
    ] );
    const material = new ShaderMaterial({
      extensions: {
        derivatives: true,
      },
      side: DoubleSide,
      uniforms: {
        time: {value: 0},
        mousePos: {value: this.mousePos},
        pixels:{
          value: new Vector2(window.innerWidth, window.innerHeight)
        },
        uvRate1: {
          value: new Vector2(1,1)
        },
        ...lightsUniforms,
      },
      vertexShader: ParticlesVertex,
      fragmentShader: ParticlesFrag,
      lights: true
    })
    
    material.wireframe = true;
    const mesh = new Points(geometry, material)
    mesh.geometry.rotateX(-1.5707963267948963);
    mesh.geometry.rotateY(MathUtils.degToRad(-15));
    mesh.geometry.scale(40,40,40);
    mesh.geometry.center();
    mesh.updateMatrix()
    return mesh;
  }
  makePlane(){   
    this.setBoundingBox();
    const wX = this.boundingBox.max.x - this.boundingBox.min.x;
    const wY = this.boundingBox.max.y - this.boundingBox.min.y;
    const geo = new PlaneGeometry(wX*1.5, wY*1.5, 1, 1);
    const mat = new MeshBasicMaterial({visible: false});
    const mesh = new Mesh(geo, mat);
    return mesh;
  }
  setBoundingBox(){
    this.figureMesh.geometry.computeBoundingBox();
    this.boundingBox.copy(this.figureMesh.geometry.boundingBox);
    // this.figureMesh.updateMatrixWorld(true); 
    this.boundingBox.applyMatrix4( this.figureMesh.matrixWorld );
  }
  onRender(clock: Clock, reycaster: Raycaster){
    const intersects = reycaster.intersectObject(this.plane, true);
    if(intersects.length){
      this.mousePos = intersects[0].point;
      this.figureMesh.material.uniforms.mousePos.value = this.mousePos;
    }
    this.figureMesh.material.uniforms.time.value = clock.getElapsedTime();
  }
  onResize(){
    // this.mesh.material.uniforms.pixels.value = new Vector2(window.innerWidth, window.innerHeight);
  }
  setupGui(gui: GUI){

  }
}