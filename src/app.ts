import {WebGLRenderer, Scene, PerspectiveCamera, CameraHelper, Color, sRGBEncoding, PCFSoftShadowMap, Clock, BoxBufferGeometry, MeshPhongMaterial, Mesh, AxesHelper, Light, PlaneBufferGeometry, MathUtils, MeshBasicMaterial, Fog, AudioListener, Audio, Raycaster} from 'three';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls.js';
import Stats from 'three/examples/jsm/libs/stats.module.js';
import autoBind from 'auto-bind';
import {GUI} from 'dat.gui';
import LightGroup from './LightGroup';
import Model from './model';

export default class App{
  container: HTMLDivElement;
  renderer: WebGLRenderer;
  scene: Scene;
  camera: PerspectiveCamera;
  cameraHelper: CameraHelper;
  orbitControl: OrbitControls;
  clock: Clock;
  stats: Stats;
  cube: Mesh;
  model = new Model();
  lightGroup: LightGroup;
  logCameraPosition = false;
  listener: AudioListener;
  raycaster =  new Raycaster();
  mouse = {x:0,y:0};
  constructor(){
    autoBind(this);
    (async ()=>{
      await Promise.all([this.model.load()]);
      this.initThree();
      this.lightGroup = new LightGroup(this.scene);
      this.model.init(this.scene);
      this.setupSceneHelpers();
      // this.addCube();
      // this.addPlane();
      this.setupMouse();
      // this.addFog();
      this.setupGUI();
      this.render();
      window.addEventListener('resize', this.onResize);
    })()  
  }
  initThree(){
    this.container = document.querySelector('div#container');
    this.scene = new Scene();
    this.scene.background = new Color(0x61cac6);

    this.camera = new PerspectiveCamera(45, window.innerWidth / window.innerHeight, 1, 1000);
    this.camera.position.set(46, 75, 145);
    const canvas = document.createElement('canvas');
    const context = canvas.getContext( 'webgl', { alpha: false } );

    this.renderer = new WebGLRenderer( { canvas, context, antialias: true } );
    this.renderer.setPixelRatio( window.devicePixelRatio );
    this.renderer.setSize( window.innerWidth, window.innerHeight );
    this.renderer.outputEncoding = sRGBEncoding;
    this.renderer.shadowMap.enabled = true;
    this.renderer.shadowMap.type = PCFSoftShadowMap;

    this.container.appendChild(this.renderer.domElement);
  }
  onResize(){
    this.updateCamera();
    this.model.onResize();
    this.renderer.setSize( window.innerWidth, window.innerHeight );
  }
  updateCamera(){
    this.camera.aspect = window.innerWidth / window.innerHeight;
    this.camera.updateProjectionMatrix();
  }
  setupSceneHelpers(){
    this.orbitControl = new OrbitControls(this.camera, this.renderer.domElement);
    this.orbitControl.maxDistance = 300;
    this.clock = new Clock();
    this.stats = new Stats();
    const axesHelper = new AxesHelper( 20 );
    this.scene.add(axesHelper);
    document.body.appendChild(this.stats.dom);
  }
  setupMouse(){
    window.addEventListener('mousemove', e=>{
      this.mouse.x = ( e.clientX / window.innerWidth ) * 2 - 1;
      this.mouse.y = - ( e.clientY / window.innerHeight ) * 2 + 1;
    })
  }
  addCube(){
    const cubeSize = 1;
    const geometry = new BoxBufferGeometry(cubeSize, cubeSize, cubeSize);
    const material = new MeshPhongMaterial({color: '#ff00ff'});
    const mesh = new Mesh(geometry, material);
    mesh.castShadow = true
    this.cube = mesh;
    this.scene.add(mesh);
  }
  addPlane(){
    const planeSize = 300;
    const repeats = planeSize / 2;
    const planeGeo = new PlaneBufferGeometry(planeSize, planeSize);
    const planeMat = new MeshPhongMaterial({color: '#377371'})
    const mesh = new Mesh(planeGeo, planeMat);
    mesh.receiveShadow = true;
    mesh.rotation.x = MathUtils.degToRad(-90)
    this.scene.add(mesh)
  }
  addFog(){
    this.scene.fog = new Fog(this.scene.background as Color, 93, 177);
  }
  setupGUI(){
    const gui = new GUI();
    gui.add({['Log_Camera_Position']: false},'Log_Camera_Position').onChange(log=>this.logCameraPosition = log)
    this.lightGroup.setupGui(gui);
    if(this.scene.fog){
      const fogFolder = gui.addFolder('Fog')
      fogFolder.add(this.scene.fog, 'far', 100, 500, 1);
      fogFolder.add(this.scene.fog, 'near', 0, 300, 1);
    }
  }
  render(){
    this.raycaster.setFromCamera(this.mouse, this.camera);
    this.model.onRender(this.clock, this.raycaster);
    this.lightGroup.onRender(this.clock);
    this.camera.updateProjectionMatrix();
    this.stats.update();
    this.orbitControl.update();
    this.renderer.render( this.scene, this.camera );
    

    if(this.logCameraPosition){
      console.log(this.camera.position);
    }

    requestAnimationFrame(this.render);
  }
}

new App();